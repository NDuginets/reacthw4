import React, { Component } from 'react';
import './App.css';
import List from './List';
import Artist from './Artist';
import Album from './Album';
import NotFound from './NotFound';
import Trubl from './Trubl';



import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;


class App extends Component{
  state = {
    myMusic: [],
    loading: false
  }


  doFetch = () => {
    fetch("http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2").then(function(response) {
    return response.json();
    }).then(data => {
      this.setState({myMusic: data,loading: true});   
    });
  };

  componentDidMount(){
    this.doFetch();
  }

  render(){
    if (this.state.loading !== false) {
          return(
            <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
              <div>
                <Switch>
                        
                        <Route path="/list" exact render={() => {             
                            return <List myMusic={this.state.myMusic} />
                        }
                        } />   
                        <Route path="/list/:id" exact render={({match}) => { 
                            return <Artist  artist={this.state.myMusic[match.params.id]} params={match}/>
                        }
                        } />  
                        <Route path="/list/:id/:album" exact render={({match}) => { 
                            console.log(match)
                            return <Album album={this.state.myMusic[match.params.id].album[match.params.album]} params={match}/>
                        }
                        } />  
                        <Redirect from='/' to='/list'/>
                        <Route component={NotFound} />
                </Switch>
              </div>
            </BrowserRouter>
          )
    } else {
      return (<Trubl> </Trubl>)
    }
  }
}



export default App;
