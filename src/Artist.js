import React, { Component } from 'react';
import './App.css';
import './Disqus.svg';


import { Link}  from 'react-router-dom';
class Artist extends Component {

  countSongsAndTime = (item) => {
   let allTime = {
      minutes: 0
      };

      for (let i=0; i < item.compositions.length;i++) 
        {
          let tmp = item.compositions[i].duration.split(':'); 
          allTime.minutes += +tmp[0];
        }
    return (<div> i have {item.compositions.length} songs and all duration is {allTime.minutes} minutes </div>)
  };

    
  render() {
    let {artist, params} = this.props;
      return (
        <div>
          <h1> I`m Artist and my name is {artist.name}</h1>
          <h1> That is my albums</h1>
          <div>
              {        
                artist.album.map((item, index) => 
                      {
                      let link = `${params.url}/${index}`;     
                      let someInfo = this.countSongsAndTime(item);
                      return  (<div key={index}> <Link  className="navigateLinks" to={link}>{item.name}</Link> 
                              {someInfo} </div> )
                      })      
              } 
              <Link className="navigateLinks" to={'/list'}> Bact to List</Link>      
          </div>
        </div>
      );
  }
}

export default Artist;