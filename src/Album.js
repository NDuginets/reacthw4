import React, { Component } from 'react';
import './App.css';
import { Link}  from 'react-router-dom';
import StarRating from './StartRating.js'

class Album extends Component {
  render() {
    let {album, params} = this.props;   
      return (
      <div>
        <h1> I`m Album and my name is {album.name}</h1>
        <div>
              {       
               album.compositions.map((item, index) => {       
                      return (<div key={index}> {item.name} <StarRating name={item.name}/></div>)
                      })      
              }  

              <Link className="navigateLinks" to={'/list/'+params.params.id}> Bact to Artist</Link>   
        </div>
      </div>
    );
  }
}

export default Album;
